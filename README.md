# Producer

A simple script to publish information about new release from CI/CD system to a message queue

## Prerequisites

The script requires Pika library:

```
apt-get install python3-pika
```

or

```
pip3 install pika
```

## Configuration

Set MQ connection parameters:

```
MQ_HOST = "host"
MQ_PORT =  5672
MQ_USER = "script"
MQ_PASS = "scriptpass"
MQ_VIRTUALHOST = "scriptvh"
```

## Usage

```
$ ./producer.py 2016.20 2012-07-08 11:14:15
```

