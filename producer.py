#!/usr/bin/env python3

import pika
import sys
import json

if len(sys.argv[1:]) < 3:
    print("Too few input arguments!")
    sys.exit(1)

MQ_HOST = ""
MQ_PORT = 
MQ_USER = ""
MQ_PASS = ""
MQ_VIRTUALHOST = ""

release_num = sys.argv[1]
datetime = sys.argv[2] + " " + sys.argv[3]

messages = []
message_events = json.dumps({"releaseNum" : release_num, "datetime" : datetime, "target" : "events"})
message_logs = json.dumps({"releaseNum" : release_num, "datetime" : datetime, "target" : "logs"})
messages.append(message_events)
messages.append(message_logs)

credentials = pika.PlainCredentials(MQ_USER, MQ_PASS)
parameters = pika.ConnectionParameters(MQ_HOST,
                                       MQ_PORT,
                                       MQ_VIRTUALHOST,
                                       credentials)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.queue_declare(queue='citomonitor')
for message in messages:
  channel.basic_publish(exchange='',
                      routing_key='citomonitor',
                      body=message)
  print(" [x] Sent " + message)

connection.close()
sys.exit(0)
